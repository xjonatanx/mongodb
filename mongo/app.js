var express = require('express');
var bodyParser = require('body-parser');
var mongoose = require('mongoose');
var methodOverride = require("method-override");
var app = express();
var cors = require('cors');
app.use(cors());

// Connection to DB
mongoose.connect('mongodb://mongodb/empleados', function (err, res) {
    if (err) throw err;
    console.log('Connected to Database');
});

// Middlewares
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(methodOverride());

// Import Models and Controllers
var models = require('./models/empleados')(app, mongoose);
var ClientCtrl = require('./controllers/empleados');
var modelsCursos = require('./models/cursos')(app, mongoose);
var cursosCtrl = require('./controllers/cursos');

var router = express.Router();
console.log("node")
// Index - Route
router.get('/', function (req, res) {
    res.send("Hola Mundo - www.programacion.com.py");
});

app.use(router);

// API routes
var api = express.Router();

api.route('/empleados')
    .get(ClientCtrl.findAll);

api.route('/agregarEmpleados')
    .get(ClientCtrl.agregar);

api.route('/empleados/:id')
    .get(ClientCtrl.findById);

api.route('/cursos')
    .get(cursosCtrl.findAll);

api.route('/agregarCursos')
    .get(cursosCtrl.agregarCursos);

app.use('/api', api);


// Start server
app.listen(3000, function () {
    console.log("Node server running on http://localhost:4000");
});