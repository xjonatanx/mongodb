var mongoose = require('mongoose');
var Empleados = mongoose.model('Empleados');
var notnull = require("util");
var multipart = require("connect-multiparty");
var multer = require("multer");

//GET - Return all registers
exports.findAll = function (req, res) {
    Empleados.find(function (err, empleado) {
        if (err) res.send(500, err.message);
        console.log('GET /empleados')
        res.jsonp(empleado);
    });
};

//GET - Return a register with specified ID
exports.findById = function (req, res) {
    var valor = req.params.id;
    var rut = valor.split(",");
    var array = [];
    Empleados.find(function (err, empleado) {
        if (err) res.send(500, err.message);
        for (var i = 0; i < rut.length; i++) {
            for (var x = 0; x < Object.keys(empleado).length; x++) {
                var nombre = empleado[x.toString()]['primerNombre'] + ' ' + empleado[x.toString()]['segundoNombre'] + ' ' + empleado[x.toString()]['apellidoPaterno'] + ' ' + empleado[x.toString()]['apellidoMaterno'];
                if (empleado[x.toString()]['rut'] === rut[i] || nombre.toLowerCase() === rut[i].toString().toLowerCase()) {
                    array.push(empleado[x.toString()])
                    //res.jagregar();sonp(array);
                }
            }
        }
        res.jsonp(array);
    });
};

exports.agregar = function (req, res) {

    "use strict";
    const excelToJson = require("convert-excel-to-json");

    const result = excelToJson({
        sourceFile: "./uploads/userPhoto.xlsx",
        header: {
            rows: 4
        },
        columnToKey: {
            A: "PrimerNombre",
            B: "SegundoNombre",
            C: "ApellidoPaterno",
            D: "ApellidoMaterno",
            E: "Rut",
            F: "Cargo",
            G: "SPA",
            H: "Numero Contrato",
            I: "Empresa",
            J: "Gerencia",
            K: "Estado Aislamiento y Bloqueo",
            L: "Aislamiento y Bloqueo",
            M: "Estado Induccion PT",
            N: "Induccion PT",
            O: "Estado Alta Tension",
            P: "Alta Tension",
            Q: "Estado Trabajos en Altura",
            R: "Trabajos en Altura",
            S: "Estado Sustancias Quimicas",
            T: "Sustancias Quimicas",
            U: "Estado Trabajos en Caliente",
            V: "Trabajos en Caliente",
            W: "Estado Espacios Confinados",
            X: "Espacios Confinados",
            Y: "Estado Excavacion y Zanjas",
            Z: "Excavacion y Zanjas",
            AA: "Estado Inspeccion a las Condiciones Fisicas de Funcionamiento",
            AB:
                "Inspeccion a las Condiciones Fisicas de Funcionamiento",
            AC: "Estado OPT",
            AD: "OPT",
            AE: "Estado OPS",
            AF: "OPS",
            AG: "Estado Toma 5",
            AH: "Toma 5",
            AI: "Estado Conduccion a la Defensiva",
            AJ: "Conduccion a la Defensiva",
            AK: "Estado Riesgo Mat Operadores",
            AL: "Riesgo Mat Operadores",
            AM: "Estado Riesgo Mat Supervisores",
            AN: "Riesgo Mat Supervisores",
            AO: "Estado Riesgo Mat Ejecutivos",
            AP: "Riesgo Mat Ejecutivos",
            AQ: "Estado Inventario de Riesgos",
            AR: "Inventario de Riesgos",
            AS: "Estado ACN",
            AT: "ACN",
            AU: "Estado OVCC",
            AV: "OVCC",
            AW: "Estado OPT (FLP)",
            AX: "OPT (FLP)",
            AY: "Estado Take Time Talk",
            AZ: "Take Time Talk",
            BA: "Estado Gestion Ambiental",
            BB: "Gestion Ambiental",
            BC: "Estado Sustancias Peligrosas",
            BD: "Sustancias Peligrosas",
            BE: "Estado Manejo de Residuos",
            BF: "Manejo de Residuos",
            BG: "Estado Biodiversidad",
            BH: "Biodiversidad",
            BI: "Estado Plan CEM",
            BJ: "Plan CEM",
            BK: "Estado Cond Climáticas Adv.",
            BL: "Cond Climáticas Adv",
            BM: "Estado Control de Incendios",
            BN: "Control de Incendios",
            BO: "Estado Preparacion Emergencias",
            BP: "Preparacion Emergencias",
            BQ: "Estado Radiacion UV",
            BR: "Radiacion UV",
            BS: "Estado Proteccion Auditiva",
            BT: "Proteccion Auditiva",
            BU: "Estado Proteccion Respiratoria",
            BV: "Proteccion Respiratoria",
            BW: "Estado Primeros Auxilios",
            BX: "Primeros Auxilios",
            BY: "Estado Higiene Ocupacional",
            BZ: "Higiene Ocupacional",
            CA: "Estado Liderazgo en Terreno",
            CB: "Liderazgo en Terreno",
            CC: "Estado Adm Riesgos Mat GLD017",
            CD: "Adm. Riesgos Mat. GLD017",
            CE: "Estado Gestion de Eventos HSEC",
            CF: "Gestion de Eventos HSEC",
            CG: "Estado Gestion de DDHH",
            CH: "Gestion de DDHH",
            CI: "Estado Modelo Operativo BHP",
            CJ: "Modelo Operativo BHP",
            CK: "Estado Sistema Gestion HSEC",
            CL: "Sistema Gestion HSEC",
            CM: "Estado Gestion de Permisos",
            CN: "Gestion de Permisos"
        }
    });

    const soloCursos = excelToJson({
        sourceFile: "./uploads/userPhoto.xlsx",
        header: {
            rows: 4
        },
        columnToKey: {
            K: "Estado Aislamiento y Bloqueo",
            L: "Aislamiento y Bloqueo",
            M: "Estado Induccion PT",
            N: "Induccion PT",
            O: "Estado Alta Tension",
            P: "Alta Tension",
            Q: "Estado Trabajos en Altura",
            R: "Trabajos en Altura",
            S: "Estado Sustancias Quimicas",
            T: "Sustancias Quimicas",
            U: "Estado Trabajos en Caliente",
            V: "Trabajos en Caliente",
            W: "Estado Espacios Confinados",
            X: "Espacios Confinados",
            Y: "Estado Excavacion y Zanjas",
            Z: "Excavacion y Zanjas",
            AA: "Estado Inspeccion a las Condiciones Fisicas de Funcionamiento",
            AB:
                "Inspeccion a las Condiciones Fisicas de Funcionamiento",
            AC: "Estado OPT",
            AD: "OPT",
            AE: "Estado OPS",
            AF: "OPS",
            AG: "Estado Toma 5",
            AH: "Toma 5",
            AI: "Estado Conduccion a la Defensiva",
            AJ: "Conduccion a la Defensiva",
            AK: "Estado Riesgo Mat Operadores",
            AL: "Riesgo Mat Operadores",
            AM: "Estado Riesgo Mat Supervisores",
            AN: "Riesgo Mat Supervisores",
            AO: "Estado Riesgo Mat Ejecutivos",
            AP: "Riesgo Mat Ejecutivos",
            AQ: "Estado Inventario de Riesgos",
            AR: "Inventario de Riesgos",
            AS: "Estado ACN",
            AT: "ACN",
            AU: "Estado OVCC",
            AV: "OVCC",
            AW: "Estado OPT (FLP)",
            AX: "OPT (FLP)",
            AY: "Estado Take Time Talk",
            AZ: "Take Time Talk",
            BA: "Estado Gestion Ambiental",
            BB: "Gestion Ambiental",
            BC: "Estado Sustancias Peligrosas",
            BD: "Sustancias Peligrosas",
            BE: "Estado Manejo de Residuos",
            BF: "Manejo de Residuos",
            BG: "Estado Biodiversidad",
            BH: "Biodiversidad",
            BI: "Estado Plan CEM",
            BJ: "Plan CEM",
            BK: "Estado Cond Climáticas Adv.",
            BL: "Cond Climáticas Adv",
            BM: "Estado Control de Incendios",
            BN: "Control de Incendios",
            BO: "Estado Preparacion Emergencias",
            BP: "Preparacion Emergencias",
            BQ: "Estado Radiacion UV",
            BR: "Radiacion UV",
            BS: "Estado Proteccion Auditiva",
            BT: "Proteccion Auditiva",
            BU: "Estado Proteccion Respiratoria",
            BV: "Proteccion Respiratoria",
            BW: "Estado Primeros Auxilios",
            BX: "Primeros Auxilios",
            BY: "Estado Higiene Ocupacional",
            BZ: "Higiene Ocupacional",
            CA: "Estado Liderazgo en Terreno",
            CB: "Liderazgo en Terreno",
            CC: "Estado Adm Riesgos Mat GLD017",
            CD: "Adm. Riesgos Mat. GLD017",
            CE: "Estado Gestion de Eventos HSEC",
            CF: "Gestion de Eventos HSEC",
            CG: "Estado Gestion de DDHH",
            CH: "Gestion de DDHH",
            CI: "Estado Modelo Operativo BHP",
            CJ: "Modelo Operativo BHP",
            CK: "Estado Sistema Gestion HSEC",
            CL: "Sistema Gestion HSEC",
            CM: "Estado Gestion de Permisos",
            CN: "Gestion de Permisos"
        }
    });

    const cursos1 = excelToJson({
        sourceFile: "./uploads/userPhoto.xlsx",
        sheets: [
            {
                name: "Consolidado",
                range: "AA3:CN3"
            }
        ]
    });

    const cursos2 = excelToJson({
        sourceFile: "./uploads/userPhoto.xlsx",
        sheets: [
            {
                name: "Consolidado",
                range: "K3:Z3"
            }
        ]
    });

    var cursos = [];
    cursos.push(cursos2["Consolidado"]["0"]["K"]);
    cursos.push(cursos2["Consolidado"]["0"]["M"]);
    cursos.push(cursos2["Consolidado"]["0"]["O"]);
    cursos.push(cursos2["Consolidado"]["0"]["Q"]);
    cursos.push(cursos2["Consolidado"]["0"]["S"]);
    cursos.push(cursos2["Consolidado"]["0"]["U"]);
    cursos.push(cursos2["Consolidado"]["0"]["W"]);
    cursos.push(cursos2["Consolidado"]["0"]["Y"]);
    cursos.push(cursos1["Consolidado"]["0"]["AA"]);
    cursos.push(cursos1["Consolidado"]["0"]["AC"]);
    cursos.push(cursos1["Consolidado"]["0"]["AE"]);
    cursos.push(cursos1["Consolidado"]["0"]["AG"]);
    cursos.push(cursos1["Consolidado"]["0"]["AI"]);
    cursos.push(cursos1["Consolidado"]["0"]["AK"]);
    cursos.push(cursos1["Consolidado"]["0"]["AM"]);
    cursos.push(cursos1["Consolidado"]["0"]["AO"]);
    cursos.push(cursos1["Consolidado"]["0"]["AQ"]);
    cursos.push(cursos1["Consolidado"]["0"]["AS"]);
    cursos.push(cursos1["Consolidado"]["0"]["AU"]);
    cursos.push(cursos1["Consolidado"]["0"]["AW"]);
    cursos.push(cursos1["Consolidado"]["0"]["AY"]);
    cursos.push(cursos1["Consolidado"]["0"]["BA"]);
    cursos.push(cursos1["Consolidado"]["0"]["BC"]);
    cursos.push(cursos1["Consolidado"]["0"]["BE"]);
    cursos.push(cursos1["Consolidado"]["0"]["BG"]);
    cursos.push(cursos1["Consolidado"]["0"]["BI"]);
    cursos.push(cursos1["Consolidado"]["0"]["BK"]);
    cursos.push(cursos1["Consolidado"]["0"]["BM"]);
    cursos.push(cursos1["Consolidado"]["0"]["BO"]);
    cursos.push(cursos1["Consolidado"]["0"]["BQ"]);
    cursos.push(cursos1["Consolidado"]["0"]["BS"]);
    cursos.push(cursos1["Consolidado"]["0"]["BU"]);
    cursos.push(cursos1["Consolidado"]["0"]["BW"]);
    cursos.push(cursos1["Consolidado"]["0"]["BY"]);
    cursos.push(cursos1["Consolidado"]["0"]["CA"]);
    cursos.push(cursos1["Consolidado"]["0"]["CC"]);
    cursos.push(cursos1["Consolidado"]["0"]["CE"]);
    cursos.push(cursos1["Consolidado"]["0"]["CG"]);
    cursos.push(cursos1["Consolidado"]["0"]["CI"]);
    cursos.push(cursos1["Consolidado"]["0"]["CK"]);
    cursos.push(cursos1["Consolidado"]["0"]["CM"]);

    function quitaAcentos(str) {
        for (var i = 0; i < str.length; i++) {
            if (str.charAt(i) == "á") str = str.replace(/á/, "a");
            if (str.charAt(i) == "é") str = str.replace(/é/, "e");
            if (str.charAt(i) == "í") str = str.replace(/í/, "i");
            if (str.charAt(i) == "ó") str = str.replace(/ó/, "o");
            if (str.charAt(i) == "ú") str = str.replace(/ú/, "u");
            if (str.charAt(i) == "Á") str = str.replace(/Á/, "A");
            if (str.charAt(i) == "É") str = str.replace(/É/, "E");
            if (str.charAt(i) == "Í") str = str.replace(/Í/, "I");
            if (str.charAt(i) == "Ó") str = str.replace(/Ó/, "O");
            if (str.charAt(i) == "Ú") str = str.replace(/Ú/, "U");
        }
        return str;
    }

    var cursosFormato = [];
    for (var i = 0; i < cursos.length; i++) {
        var cadena = quitaAcentos(cursos[i]);
        var otro = cadena.replace(/\./g, '');
        cursosFormato.push(otro);
    }

    for (var i = 0; i < Object.keys(result['Consolidado']).length; i++) {
        for (var x = 0; x < cursosFormato.length; x++) {
            if (result['Consolidado'][i.toString()]["Estado " + cursosFormato[x].toString()] === "Pendiente") {
                delete result['Consolidado'][i.toString()]["Estado " + cursosFormato[x].toString()];
                delete result['Consolidado'][i.toString()][cursosFormato[x]];
                delete soloCursos['Consolidado'][i.toString()]["Estado " + cursosFormato[x].toString()];
                delete soloCursos['Consolidado'][i.toString()][cursosFormato[x]];
            } else {
                delete result['Consolidado'][i.toString()]["Estado " + cursosFormato[x].toString()];
                delete soloCursos['Consolidado'][i.toString()]["Estado " + cursosFormato[x].toString()];
            }
        }
    }
    //res.jsonp(result);
    

    //res.jsonp(cursosFormato);
    for (let i = 0; i < Object.keys(result['Consolidado']).length; i++) {
            //console.log(result['Consolidado'][i.toString()]);
            var nuevoEmpleado = new Empleados({
                primerNombre: result['Consolidado'][i.toString()]['PrimerNombre'],
                segundoNombre: result['Consolidado'][i.toString()]['SegundoNombre'],
                apellidoPaterno: result['Consolidado'][i.toString()]['ApellidoPaterno'],
                apellidoMaterno: result['Consolidado'][i.toString()]['ApellidoMaterno'],
                rut: result['Consolidado'][i.toString()]['Rut'],
                cargo: result['Consolidado'][i.toString()]['Cargo'],
                spa: result['Consolidado'][i.toString()]['SPA'],
                numeroContrato: result['Consolidado'][i.toString()]['Numero Contrato'],
                empresa: result['Consolidado'][i.toString()]['Empresa'],
                gerencia: result['Consolidado'][i.toString()]['Gerencia'],
                cursos: soloCursos['Consolidado'][i.toString()]
            });
        nuevoEmpleado.save(function (err, nuevoEmpleado) {
            if (err){
                return console.log(err.message);
            } else {
                console.log("INSERT empleado "+ i)
            }
        });
    }
}