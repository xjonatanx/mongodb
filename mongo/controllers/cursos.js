var mongoose = require('mongoose');
var Cursos = mongoose.model('Cursos');
var notnull = require("util");
var multipart = require("connect-multiparty");
var multer = require("multer");

//GET - Return all cursos
exports.findAll = function (req, res) {
    Cursos.find(function (err, cursos) {
        if (err) res.send(500, err.message);
        console.log('GET /cursos')
        res.status(200).jsonp(cursos);
    });
};

exports.agregarCursos = function (req, res) {
    "use strict";
    const excelToJson = require("convert-excel-to-json");

    const cursos1 = excelToJson({
        sourceFile: "./uploads/userPhoto.xlsx",
        sheets: [
            {
                name: "Consolidado",
                range: "AA3:CN3"
            }
        ]
    });

    const cursos2 = excelToJson({
        sourceFile: "./uploads/userPhoto.xlsx",
        sheets: [
            {
                name: "Consolidado",
                range: "K3:Z3"
            }
        ]
    });

    var cursos = [];
    cursos.push(cursos2["Consolidado"]["0"]["K"]);
    cursos.push(cursos2["Consolidado"]["0"]["M"]);
    cursos.push(cursos2["Consolidado"]["0"]["O"]);
    cursos.push(cursos2["Consolidado"]["0"]["Q"]);
    cursos.push(cursos2["Consolidado"]["0"]["S"]);
    cursos.push(cursos2["Consolidado"]["0"]["U"]);
    cursos.push(cursos2["Consolidado"]["0"]["W"]);
    cursos.push(cursos2["Consolidado"]["0"]["Y"]);
    cursos.push(cursos1["Consolidado"]["0"]["AA"]);
    cursos.push(cursos1["Consolidado"]["0"]["AC"]);
    cursos.push(cursos1["Consolidado"]["0"]["AE"]);
    cursos.push(cursos1["Consolidado"]["0"]["AG"]);
    cursos.push(cursos1["Consolidado"]["0"]["AI"]);
    cursos.push(cursos1["Consolidado"]["0"]["AK"]);
    cursos.push(cursos1["Consolidado"]["0"]["AM"]);
    cursos.push(cursos1["Consolidado"]["0"]["AO"]);
    cursos.push(cursos1["Consolidado"]["0"]["AQ"]);
    cursos.push(cursos1["Consolidado"]["0"]["AS"]);
    cursos.push(cursos1["Consolidado"]["0"]["AU"]);
    cursos.push(cursos1["Consolidado"]["0"]["AW"]);
    cursos.push(cursos1["Consolidado"]["0"]["AY"]);
    cursos.push(cursos1["Consolidado"]["0"]["BA"]);
    cursos.push(cursos1["Consolidado"]["0"]["BC"]);
    cursos.push(cursos1["Consolidado"]["0"]["BE"]);
    cursos.push(cursos1["Consolidado"]["0"]["BG"]);
    cursos.push(cursos1["Consolidado"]["0"]["BI"]);
    cursos.push(cursos1["Consolidado"]["0"]["BK"]);
    cursos.push(cursos1["Consolidado"]["0"]["BM"]);
    cursos.push(cursos1["Consolidado"]["0"]["BO"]);
    cursos.push(cursos1["Consolidado"]["0"]["BQ"]);
    cursos.push(cursos1["Consolidado"]["0"]["BS"]);
    cursos.push(cursos1["Consolidado"]["0"]["BU"]);
    cursos.push(cursos1["Consolidado"]["0"]["BW"]);
    cursos.push(cursos1["Consolidado"]["0"]["BY"]);
    cursos.push(cursos1["Consolidado"]["0"]["CA"]);
    cursos.push(cursos1["Consolidado"]["0"]["CC"]);
    cursos.push(cursos1["Consolidado"]["0"]["CE"]);
    cursos.push(cursos1["Consolidado"]["0"]["CG"]);
    cursos.push(cursos1["Consolidado"]["0"]["CI"]);
    cursos.push(cursos1["Consolidado"]["0"]["CK"]);
    cursos.push(cursos1["Consolidado"]["0"]["CM"]);

    function quitaAcentos(str) {
        for (var i = 0; i < str.length; i++) {
            if (str.charAt(i) == "á") str = str.replace(/á/, "a");
            if (str.charAt(i) == "é") str = str.replace(/é/, "e");
            if (str.charAt(i) == "í") str = str.replace(/í/, "i");
            if (str.charAt(i) == "ó") str = str.replace(/ó/, "o");
            if (str.charAt(i) == "ú") str = str.replace(/ú/, "u");
            if (str.charAt(i) == "Á") str = str.replace(/Á/, "A");
            if (str.charAt(i) == "É") str = str.replace(/É/, "E");
            if (str.charAt(i) == "Í") str = str.replace(/Í/, "I");
            if (str.charAt(i) == "Ó") str = str.replace(/Ó/, "O");
            if (str.charAt(i) == "Ú") str = str.replace(/Ú/, "U");
        }
        return str;
    }

    var cursosFormato = [];
    for (var i = 0; i < cursos.length; i++) {
        var cadena = quitaAcentos(cursos[i]);
        var otro = cadena.replace(/\./g, '');
        cursosFormato.push(otro);
    }

    for (let i = 0; i < cursosFormato.length; i++) {
        var nuevoCurso = new Cursos({
            nombreCurso: cursosFormato[i].toString()
        });
        nuevoCurso.save(function (err, nuevoCurso) {
            if (err) return res.send(500, err.message);
            console.log('INSERT /curso ' + i)
        });
    }
};