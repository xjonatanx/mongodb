var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Cursos = new Schema({
    nombreCurso: { type: String },
});

module.exports = mongoose.model('Cursos', Cursos);