var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Empleados = new Schema({
    primerNombre: { type: String },
    segundoNombre: { type: String },
    apellidoPaterno: { type: String },
    apellidoMaterno: { type: String },
    rut: { type: String },
    cargo: { type: String },
    spa: { type: String },
    numeroContrato: { type: String },
    empresa: { type: String },
    gerencia: { type: String },
    cursos: { type: Object }
});

module.exports = mongoose.model('Empleados', Empleados);